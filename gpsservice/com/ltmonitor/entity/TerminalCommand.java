﻿package com.ltmonitor.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;

import net.sourceforge.jtds.jdbc.DateTime;

//终端命令
@Entity
@Table(name="TerminalCommand")
@org.hibernate.annotations.Proxy(lazy = false)
@Inheritance(strategy=InheritanceType.TABLE_PER_CLASS) 
public class TerminalCommand extends TenantEntity {
	public static final String STATUS_NEW = "New";
	public static final String STATUS_PROCESSING = "Processing";
	public static final String STATUS_INVALID = "Invalid";
	public static final String STATUS_SUCCESS = "Success";
	public static final String STATUS_FAILED = "Failed";
	public static final String STATUS_OFFLINE = "Offline";
	public static final String STATUS_NOT_SUPPORT = "NotSupport";
	
	//owner属性值:政府平台，terminal,用户登录名 三种属性
	public static final String FROM_GOV = "政府平台";
	public static final String FROM_TERMINAL = "terminal";
	
	public TerminalCommand() {
		setCreateDate(new java.util.Date());
		setStatus(STATUS_NEW);
	}
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "cmdId", unique = true, nullable = false)
	
	private int entityId;
	@Override
	public final int getEntityId() {
		return entityId;
	}
	@Override
	public final void setEntityId(int value) {
		entityId = value;
	}
	/**
	 * 车辆Id
	 */
	private int vehicleId;


	// 终端ID号

	@Column(name = "simNo")
	private String simNo;

	public final String getSimNo() {
		return simNo;
	}

	public final void setSimNo(String value) {
		simNo = value;
	}

	// 车牌号
	@Column(name = "plateNo")
	private String plateNo;

	public final String getPlateNo() {
		return plateNo;
	}

	public final void setPlateNo(String value) {
		plateNo = value;
	}

	// 类别 命令的大类别
	@Column(name = "cmdType")
	private int cmdType;

	public final int getCmdType() {
		return cmdType;
	}

	public final void setCmdType(int value) {
		cmdType = value;
	}

	// 命令数据中的命令字或标志位
	@Column(name = "cmd")
	private String cmd;

	public final String getCmd() {
		return cmd;
	}

	public final void setCmd(String value) {
		cmd = value;
	}

	// 数据，多个参数的时候，使用;隔开
	@Column(name = "cmdData")
	private String cmdData;


	// 命令状态
	@Column(name = "status")
	private String status;

	public final String getStatus() {
		return status;
	}

	public final void setStatus(String value) {
		status = value;
	}

	// 命令下发的流水号
	private int SN;

	@Column(name = "SN")
	public final int getSN() {
		return SN;
	}

	public final void setSN(int value) {
		SN = value;
	}

	// 区域设置时，关联的区域数据(Enclosure类)
	// public int EnclosureId { get; set; }

	// 命令的发送者
	@Column(name = "userId")
	private int userId;

	public final int getUserId() {
		return userId;
	}

	public final void setUserId(int value) {
		userId = value;
	}
	//命令执行时间
	public Date getUpdateDate() {
		return updateDate;
	}
	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public String getCmdData() {
		return cmdData;
	}
	public void setCmdData(String cmdData) {
		this.cmdData = cmdData;
	}

	public int getVehicleId() {
		return vehicleId;
	}
	public void setVehicleId(int vehicleId) {
		this.vehicleId = vehicleId;
	}

	private Date updateDate;
}