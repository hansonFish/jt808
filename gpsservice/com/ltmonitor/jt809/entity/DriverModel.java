package com.ltmonitor.jt809.entity;

import java.io.Serializable;

public class DriverModel  implements Serializable{
	private String plateNo;
	private int plateColor;
	//姓名
	private String driverName;
	//驾驶身份证
	private String driverId;
	//资格证
	private String license;
	//组织机构代码
	private String orgName;
	
	public DriverModel()
	{
		plateNo = "��A78903";
		plateColor = 1;
		driverName = "����";
		setDriverId("12345678890");
		setLicense("3212341234");
		orgName = "������������";
	}

	public String getDriverName() {
		return this.driverName;
	}

	public void setDriverName(String driverName) {
		this.driverName = driverName;
	}

	public String getOrgName() {
		return this.orgName;
	}

	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}

	public String getPlateNo() {
		return plateNo;
	}

	public void setPlateNo(String plateNo) {
		this.plateNo = plateNo;
	}

	public int getPlateColor() {
		return plateColor;
	}

	public void setPlateColor(int plateColor) {
		this.plateColor = plateColor;
	}

	public void setDriverId(String driverId) {
		this.driverId = driverId;
	}

	public String getDriverId() {
		return driverId;
	}

	public void setLicense(String license) {
		this.license = license;
	}

	public String getLicense() {
		return license;
	}
}
