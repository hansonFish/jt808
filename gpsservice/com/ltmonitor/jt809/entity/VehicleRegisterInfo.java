package com.ltmonitor.jt809.entity;

import java.io.Serializable;

/**
 * 上级平台要求提供的车辆注册信息
 * @author DELL
 *
 */
public class VehicleRegisterInfo implements Serializable{
	
	private String plateNo;
	
	private int plateColor;
	//平台ID
	private String plateformId;
	//终端厂商ID
	private String terminalVendorId;
	//终端型号
	private String terminalModel;
	//终端ID
	private String terminalId;
	//sim卡号
	private String simNo;

	public String getPlateNo() {
		return plateNo;
	}

	public void setPlateNo(String plateNo) {
		this.plateNo = plateNo;
	}

	public int getPlateColor() {
		return plateColor;
	}

	public void setPlateColor(int plateColor) {
		this.plateColor = plateColor;
	}

	public String getPlateformId() {
		return plateformId;
	}

	public void setPlateformId(String plateformId) {
		this.plateformId = plateformId;
	}

	public String getTerminalVendorId() {
		return terminalVendorId;
	}

	public void setTerminalVendorId(String terminalVendorId) {
		this.terminalVendorId = terminalVendorId;
	}

	public String getTerminalModel() {
		return terminalModel;
	}

	public void setTerminalModel(String terminalModel) {
		this.terminalModel = terminalModel;
	}

	public String getTerminalId() {
		return terminalId;
	}

	public void setTerminalId(String terminalId) {
		this.terminalId = terminalId;
	}

	public String getSimNo() {
		return simNo;
	}

	public void setSimNo(String simNo) {
		this.simNo = simNo;
	}

}
